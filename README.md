# TIFTEX
*C'est pas révolutionnaire mais ça fait le tiff !!!*  

Un script local (en circuit court) pour compiler du tex dans un terminal et ne plus avoir à passer par overleaf ...   

Si cela ne vous convient pas, il existe toujours [$ man rubber](https://www.man-linux-magique.net/man1/rubber.html).    

*Note:* je déconseille fortement l'utilisation de google pour une recherche `rubber latex` ou `man rubber latex` dans un contexte pro.


## Pré-requis
Au cas où ...
```
user@blabla:~/[...]$ sudo apt-get install texlive-full
```
## Commandes
```
user@blabla:~/[...]$ tiftex
A non-revolutionary TeX compiler!
COMMAND
	tiftex <tex FILE> {-c} {-u} {-o} {-x} {-b} || {-A} {-Z} {-U} {-X}
OPTIONS
	-c : compile
	-x : clear the compilation files
	-o : open .pdf file
	-u : udpdate with generated *.aux file
	-z : zip all files ie. main and contents
	-f : file discrepancy protocol
	-b : bibtex compilation (only)
MACRO
	-A := -c -o -x (all)
	-Z : zip := -c -x -g (zip)
	-U := -u -o
	-X : clear the .pdf file too !
```
### Petit truc
#### Changer le compilateur de base 
```
user@blabla:~/[...]$ touch .use_nomducompilo
```
#### Changer le compilateur de biblio
```
user@blabla:~/[...]$ touch .bib_nomdubibcompilo
```

## Installation
[Source: Run Bash script from anywhere](https://devconnected.com/how-to-run-a-bash-script/)
```
user@blabla:~/[...]$ chmod u+x tiftex
user@blabla:~/[...]$ LP=$PWD
user@blabla:~/[...]$ export PATH="$LP:$PATH"
OR
user@blabla:~/[...]$ sudo vim ~/.bashrc
		[...]
		export PATH="<path_to_script>:$PATH"
user@blabla:~/[...]$ source ~/.bashrc
user@blabla:~/[...]$ echo $PATH
user@blabla:~/[...]$ tiftex
tiftex <tex FILE> {-c} {-u} {-o} {-x} || {-a} {-z}
	-c : compile
	-x : clear
	-o : open
	-u : udpdate with generated *.aux file
	-g : zip files (main and contents)
	-a : all := -c -o -x
	-z : zip := -c -x -g
```
**Remarque:** si j'ai oublié de retirer l'extension ...
```
user@blabla:~/[...]$ mv tiftex.sh tiftex
```

## Mantras
```
cd existing_repo
git remote add origin https://gitlab.univ-nantes.fr/latif-m/tiftex.git
git pull origin main
[...]
git branch -M main
git push -uf origin main
```
En fait, je devais réviser mon bash et en même temps faire des planches et j’en avais marre d’avoir à taper plusieurs commandes pour obtenir un résultat… Adapté de [Script for Compiling Latex with Bibtex](https://ezbc.me/compiling-latex-with-bibtex) par M.Latif